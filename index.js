class Tab {
  constructor(arr) {
    this.tabsArr = arr;
    this.createStructure(this.tabsArr);
    this.addEventListeners();
  }
  createStructure(tabs) {
    this.entireDiv = document.createElement('div');
    this.entireDiv.classList.add('tab__wrapper');
    this.Tab = document.createElement('div');
    this.Tab.classList.add('tab__items');
    for (let i = 0; i < tabs.length; i++) {
      this.Tab.appendChild(this.makeTab(tabs[i], i + 1));
    }
    this.Tab.childNodes[0].classList.add('_current');
    this.content = document.createElement('div');
    this.content.id = 'tab__content';
    this.content.textContent = tabs[0].data;
    this.entireDiv.append(this.Tab);
    this.entireDiv.append(this.content);
  }
  changeCurrent(target, nodes) {
    for (const tab of nodes) {
      if (tab === target) {
        if (tab.classList.contains('_current')) {
          return;
        }
        tab.classList.add('_current');
        this.renderContent(tab);
      } else {
        if (tab.classList.contains('_current')) {
          tab.classList.remove('_current');
        }
      }
    }
  }
  addEventListeners() {
    console.log(this.content);
    this.Tab.addEventListener('click', (event) => {
      this.changeCurrent(event.target, this.Tab.childNodes);
    });
    document.addEventListener('keydown', (event) => {
      const currentActive = document.activeElement;
      if (event.key === 'ArrowLeft') {
        currentActive.previousElementSibling.focus();
      }
      if (event.key === 'ArrowRight') {
        currentActive.nextElementSibling.focus();
      }
      if (event.key === 'Enter') {
        this.changeCurrent(currentActive, this.Tab.childNodes);
      }
    });
  }
  renderContent(tab) {
    const tabTitle = tab.textContent;
    for (const cur of this.tabsArr) {
      if (cur.title === tabTitle) {
        this.content.textContent = cur.data;
      }
    }
  }
  makeTab(tab, tabIndex) {
    const newTab = document.createElement('div');
    newTab.classList.add('tab__item');
    newTab.setAttribute('tabindex', tabIndex);
    newTab.textContent = tab.title;
    return newTab;
  }
  render() {
    return this.entireDiv;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`,
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`,
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`,
  },
]);

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render());
